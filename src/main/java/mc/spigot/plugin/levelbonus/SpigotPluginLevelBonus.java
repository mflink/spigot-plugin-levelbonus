package mc.spigot.plugin.levelbonus;

import org.bukkit.plugin.java.JavaPlugin;

import mc.spigot.plugin.levelbonus.listener.PlayerLevelChangeEventListener;
import mc.spigot.plugin.levelbonus.listener.PlayerJoinEventListener;

public class SpigotPluginLevelBonus extends JavaPlugin {
	@Override
	public void onEnable() {
		super.onEnable();
		getLogger().info("Registering Listener for Player-Logins ...");
		getServer().getPluginManager().registerEvents(new PlayerJoinEventListener(), this);
		getLogger().info("Registering Listener for Player-Level-Changes ...");
		getServer().getPluginManager().registerEvents(new PlayerLevelChangeEventListener(), this);
	}
}
