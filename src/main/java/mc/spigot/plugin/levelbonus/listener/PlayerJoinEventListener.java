package mc.spigot.plugin.levelbonus.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import mc.spigot.plugin.levelbonus.utils.PlayerLevelBonusUtil;

public class PlayerJoinEventListener implements Listener {
	@EventHandler
	public void onPlayerJoinEvent(final PlayerJoinEvent event) {
		PlayerLevelBonusUtil.giveBonuses(event.getPlayer(), event.getPlayer().getLevel());
	}
}
