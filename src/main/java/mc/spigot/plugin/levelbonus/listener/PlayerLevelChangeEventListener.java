package mc.spigot.plugin.levelbonus.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLevelChangeEvent;

import mc.spigot.plugin.levelbonus.utils.PlayerLevelBonusUtil;

public class PlayerLevelChangeEventListener implements Listener {
	@EventHandler
	public void onPlayerLevelChangeEvent(final PlayerLevelChangeEvent event) {
		PlayerLevelBonusUtil.giveBonuses(event.getPlayer(), event.getNewLevel());
	}
}
