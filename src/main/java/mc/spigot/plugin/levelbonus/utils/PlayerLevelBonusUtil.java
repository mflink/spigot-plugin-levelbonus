package mc.spigot.plugin.levelbonus.utils;

import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class PlayerLevelBonusUtil {
	private static Map<Integer, PotionEffect> bonuses = new TreeMap<>();

	static {
		bonuses.put(5, new PotionEffect(PotionEffectType.LUCK, Integer.MAX_VALUE, 1));
		bonuses.put(10, new PotionEffect(PotionEffectType.FAST_DIGGING, Integer.MAX_VALUE, 1));
		bonuses.put(15, new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, 1));
		bonuses.put(20, new PotionEffect(PotionEffectType.ABSORPTION, Integer.MAX_VALUE, 1));
		bonuses.put(25, new PotionEffect(PotionEffectType.HEALTH_BOOST, Integer.MAX_VALUE, 1));
		bonuses.put(30, new PotionEffect(PotionEffectType.REGENERATION, Integer.MAX_VALUE, 1));
		bonuses.put(35, new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE, 1));
		bonuses.put(40, new PotionEffect(PotionEffectType.WATER_BREATHING, Integer.MAX_VALUE, 1));
		bonuses.put(45, new PotionEffect(PotionEffectType.FIRE_RESISTANCE, Integer.MAX_VALUE, 1));
	}

	public static void giveBonuses(final Player player, final int level) {
		for (final Entry<Integer, PotionEffect> bonusEntry : bonuses.entrySet()) {
			if (level >= bonusEntry.getKey()) {
				player.removePotionEffect(bonusEntry.getValue().getType());
				player.addPotionEffect(bonusEntry.getValue());
			}
		}
	}
}
