# Overview
This [Spigot](https://www.spigotmc.org/wiki/spigot/)-Plugin does ...

Write a short description here to give a quick overview, what this plugin does.

# Description
This section should be used to describe this plugin in more detail e.g. what conditions it has to do something.

# Installation
* Place the resulting jar-file in the plugins-folder of your Spigot-Installation.

# Commands
This mod does not have any commands. It just works. ;-)

# <a id="Todo"></a>Todo
* Nail some todos (if any) down here.

# <a id="KnownBugs"></a>Known bugs
* Put some known bugs (if any) here.

# License
See [LICENSE](LICENSE) or the file META-INF/LICENSE shipped in the jar-file.